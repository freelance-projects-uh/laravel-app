<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('id_at_customer')->nullable();
            $table->integer('reference_id')->nullable();
            $table->string('name')->nullable();
            $table->string('category')->nullable();
            $table->string('brand')->nullable();
            $table->string('color')->nullable();
            $table->string('storage')->nullable();
            $table->string('memory')->nullable();
            $table->string('condition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
