<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Import;
use App\Product;
use App\Scrapes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class AppController extends Controller
{
    public function import(Request $request){
        {
            $upload=$request->file('upload-file');
            $filePath=$upload->getRealPath();
            //open and read
            $file=fopen($filePath, 'r');
            $header= fgetcsv($file);
            $escapedHeader=[];
            //validate
            foreach ($header as $key => $value) {
                $lheader=strtolower($value);
                $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
                array_push($escapedHeader, $escapedItem);
            }

            // in real application it maybe auth user, but for i will create new user each entry
            $user = new User();
            $user->name = Str::random(3);
            $user->email = Str::random(3).'@example.com';
            $user->password = Hash::make('secret');
            $user->save();

            $customer = new Customer();
            $customer->name = Str::random(3);
            $customer->save();

            //looping through othe columns
            while($columns=fgetcsv($file))
            {
                if($columns[0]=="")
                {
                    continue;
                }
                $data= array_combine($escapedHeader, $columns);


                $import = new Import();
                $import->user_id = $user->id;
                $import->save();

                $product = Product::find($data['productid']);
                if (!isset($product)){
                    $product = new Product();
                }
                $product->id_at_customer = $customer->id;
                $product->reference_id = $data['referenceid']??null;
                $product->name = $data['product']??$data['model']??null;
                $product->category = $data['category']??null;
                $product->brand = $data['brand']??null;
                $product->color = $data['color']??null;
                $product->storage = $data['storage']??null;
                $product->memory = $data['memory']??null;
                $product->condition = $data['condition']??null;
                $product->save();

                $scrapes = new Scrapes();
                $scrapes->import_id = $import->id;
                $scrapes->customer_id = $customer->id;
                $scrapes->product_id = $product->id;
                $scrapes->link = $data['link']??null;
                $scrapes->regular_price = $data['regularprice']??null;
                $scrapes->sale_price = $data['price']?(int)$data['price']:null;
                $scrapes->in_stock = $data['instock']??null;
                $scrapes->save();


            }
            return redirect()->route('home')->with('success','Successfully imported');


        }
    }
    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $products = Product::with('scrapes')->get();
        $columns = array('reference_id', 'product_name', 'brand', 'color', 'storage', 'memory', 'condition');
        foreach($products as $product) {
            foreach ($product->scrapes as $scrape){
                if( !in_array($scrape->customer->name. ' price', $columns)) array_push($columns,$scrape->customer->name. ' price');
            }
        }


        $callback = function() use ($products, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($products as $product) {
                $dataColumns = array($product->reference_id, $product->name, $product->brand, $product->color, $product->storage, $product->memory, $product->condition);
                foreach ($product->scrapes as $scrape){
                    array_push($dataColumns,$scrape->sale_price);
                }
                fputcsv($file, $dataColumns);
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
}
