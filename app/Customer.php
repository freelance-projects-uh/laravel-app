<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function scrapes(){
        return $this->hasMany(Scrapes::class,'customer_id');
    }
}
