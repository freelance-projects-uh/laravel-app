<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scrapes(){
        return $this->hasMany(Scrapes::class,'product_id');
    }
}
