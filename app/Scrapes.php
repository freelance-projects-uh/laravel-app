<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scrapes extends Model
{
    public function import(){
        return $this->belongsTo(Import::class,'import_id');
    }
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
}
